#!/usr/bin/make -f

all: debbugs_presentation.pdf

R ?= R

%.pdf: %.svg
	inkscape -A $@ $<
	pdfcrop $@
	mv $(dir $@)*-crop.pdf $@

%.png: %.svg
	inkscape -e $@ -d 300 $<

%.tex: %.Rnw debbugs.Rnw
	$(R) --encoding=utf-8 -e "library('knitr'); knit('$<')"

debbugs_presentation.pdf: debbugs_presentation.tex debbugs_layout.tex debbugs_layout_db.tex

%.pdf: %.tex $(wildcard *.bib) $(wildcard *.tex)
	latexmk -f -pdf -pdflatex='xelatex -shell-escape -8bit -interaction=nonstopmode %O %S' -bibtex -use-make $<
